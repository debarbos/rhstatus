package cmd

import (
	"fmt"

	"github.com/fatih/color"
	config "gitlab.com/prarit/rhstatus/internal/config"
)

type rhColor struct {
	name      string
	colorFunc func(...interface{}) string
}

var rhColors = []rhColor{
	{"black", color.New(color.FgBlack).SprintFunc()},
	{"red", color.New(color.FgRed).SprintFunc()},
	{"green", color.New(color.FgGreen).SprintFunc()},
	{"yellow", color.New(color.FgYellow).SprintFunc()},
	{"blue", color.New(color.FgBlue).SprintFunc()},
	{"magenta", color.New(color.FgMagenta).SprintFunc()},
	{"cyan", color.New(color.FgCyan).SprintFunc()},
	{"white", color.New(color.FgWhite).SprintFunc()},
	{"grey", color.New(color.FgHiBlack).SprintFunc()},
	{"hiRed", color.New(color.FgHiRed).SprintFunc()},
	{"hiGreen", color.New(color.FgHiGreen).SprintFunc()},
	{"hiYellow", color.New(color.FgHiYellow).SprintFunc()},
	{"hiBlue", color.New(color.FgHiBlue).SprintFunc()},
	{"hiMagenta", color.New(color.FgHiMagenta).SprintFunc()},
	{"hiCyan", color.New(color.FgHiCyan).SprintFunc()},
	{"hiWhite", color.New(color.FgHiWhite).SprintFunc()},
	{"BLACK", color.New(color.BgBlack).SprintFunc()},
	{"RED", color.New(color.BgRed).SprintFunc()},
	{"GREEN", color.New(color.BgGreen).SprintFunc()},
	{"YELLOW", color.New(color.BgYellow).SprintFunc()},
	{"BLUE", color.New(color.BgBlue).SprintFunc()},
	{"MAGENTA", color.New(color.BgMagenta).SprintFunc()},
	{"CYAN", color.New(color.BgCyan).SprintFunc()},
	{"WHITE", color.New(color.BgWhite).SprintFunc()},
	{"HIBLACK", color.New(color.BgHiBlack).SprintFunc()},
	{"HIRED", color.New(color.BgHiRed).SprintFunc()},
	{"HIGREEN", color.New(color.BgHiGreen).SprintFunc()},
	{"HIYELLOW", color.New(color.BgHiYellow).SprintFunc()},
	{"HIBLUE", color.New(color.BgHiBlue).SprintFunc()},
	{"HIMAGENTA", color.New(color.BgHiMagenta).SprintFunc()},
	{"HICYAN", color.New(color.BgHiCyan).SprintFunc()},
	{"HIWHITE", color.New(color.BgHiWhite).SprintFunc()},
	{"notFound", color.New(color.FgWhite).SprintFunc()},
	// do not put any new colors below this line
}

func showPalette() {
	fmt.Println("See the README.md at https://gitlab.com/prarit/rhstatus for instructions on setting colors.\n\n")
	for _, rhcolor := range rhColors {
		if rhcolor.name != "notFound" {
			fmt.Println(rhcolor.colorFunc(rhcolor.name))
		}
	}
}

func getRHColor(name string) rhColor {
	var rhcolor rhColor
	for _, rhcolor = range rhColors {
		if name == rhcolor.name {
			return rhcolor
		}
	}
	return rhcolor
}

// default colors
var red = color.New(color.FgRed).SprintFunc()
var green = color.New(color.FgGreen).SprintFunc()
var yellow = color.New(color.FgYellow).SprintFunc()
var grey = color.New(color.FgHiBlack).SprintFunc()
var white = color.New(color.FgWhite).SprintFunc()
var BLUE = color.New(color.BgBlue).SprintFunc()

func setPalette() {
	colorChoice := getRHColor(config.GetGreen())
	if colorChoice.name != "notFound" {
		green = colorChoice.colorFunc
	} else {
		fmt.Printf("Color %s for green not found.  Fix the entry in .config/rhstatus/rhstatus.toml.\n", config.GetGreen())
	}

	colorChoice = getRHColor(config.GetGrey())
	if colorChoice.name != "notFound" {
		grey = colorChoice.colorFunc
	} else {
		fmt.Printf("Color %s for grey not found.  Fix the entry in .config/rhstatus/rhstatus.toml.\n", config.GetGrey())
	}

	colorChoice = getRHColor(config.GetRed())
	if colorChoice.name != "notFound" {
		red = colorChoice.colorFunc
	} else {
		fmt.Printf("Color %s for red not found.  Fix the entry in .config/rhstatus/rhstatus.toml.\n", config.GetRed())
	}

	colorChoice = getRHColor(config.GetWhite())
	if colorChoice.name != "notFound" {
		white = colorChoice.colorFunc
	} else {
		fmt.Printf("Color %s for white not found.  Fix the entry in .config/rhstatus/rhstatus.toml.\n", config.GetWhite())
	}

	colorChoice = getRHColor(config.GetYellow())
	if colorChoice.name != "notFound" {
		yellow = colorChoice.colorFunc
	} else {
		fmt.Printf("Color %s for yellow not found.  Fix the entry in .config/rhstatus/rhstatus.toml.\n", config.GetYellow())
	}

	colorChoice = getRHColor(config.GetBLUE())
	if colorChoice.name != "notFound" {
		BLUE = colorChoice.colorFunc
	} else {
		fmt.Printf("Color %s for blue not found.  Fix the entry in .config/rhstatus/rhstatus.toml.\n", config.GetBLUE())
	}
}
