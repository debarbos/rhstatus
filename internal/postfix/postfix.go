package postfix

import (
	"fmt"
	"gitlab.com/prarit/rhstatus/internal/stack"
	"strings"
)

func removeEmptyStrings(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}

func IsLogicalOperator(str string) bool {
	if str == "or" || str == "and" || str == "==" {
		return true
	}
	return false
}

func GetLogicalOperatorWeight(op string) int {
	switch op {
	case "==":
		return 2
	case "or", "and":
		return 1
	}
	return -1
}

func HasLogicalHigherPrecedence(op1 string, op2 string) bool {
	op1Weight := GetLogicalOperatorWeight(op1)
	op2Weight := GetLogicalOperatorWeight(op2)
	return op1Weight >= op2Weight
}

func ToLogicalPostfix(input string) string {
	var stack stack.Stack // this stack ONLY contains operators

	// This is the string that is returned.
	postfix := ""

	// go substring by substring through the input string
	s := strings.Split(input, " ")
	length := len(s)
	for i := 0; i < length; i++ {
		oper := strings.TrimSpace(s[i])

		// Skip "empty" substrings
		if oper == "" {
			continue
		}

		if oper == "(" {
			// always push left bracket ( on stack
			stack.Push(oper)
		} else if oper == ")" {
			// if a right bracket ) is hit, pop stack until
			// left bracket or stack is empty.  Write
			// those substrings to the output
			for !stack.Empty() {
				str, _ := stack.Top().(string)
				if str == "(" {
					break
				}
				postfix += " " + str
				stack.Pop()
			}
			stack.Pop()
		} else if !IsLogicalOperator(oper) {
			// If substring is an operand, write it to output
			postfix += " " + oper
		} else {
			// If substring is operator, pop two elements from stack and write
			// them to the output string, perform operation and push the result
			// back onto the stack for later consumption by the next operator
			for !stack.Empty() {
				top, _ := stack.Top().(string)
				if top == "(" || !HasLogicalHigherPrecedence(top, oper) {
					break
				}
				postfix += " " + top
				stack.Pop()
			}
			stack.Push(oper)
		}
	}

	// pop the remaining operators and write them to the output string
	for !stack.Empty() {
		str, _ := stack.Pop().(string)
		postfix += " " + str
	}

	return postfix
}

func ToLogicalInfix(input string) string {
	var stack stack.Stack

	// read the string word by word
	s := strings.Split(input, " ")
	for _, oper := range s {
		if oper == "" {
			continue
		}

		if !IsLogicalOperator(oper) {
			// if operand, push
			stack.Push(oper)
		} else {
			// if operator, pop 2, combine with operator, then push
			first := fmt.Sprintf("%v", stack.Pop())
			second := fmt.Sprintf("%v", stack.Pop())
			stack.Push(fmt.Sprintf("(%s %s %s)", second, oper, first))
		}
	}

	// if end, pop for answer
	return fmt.Sprintf("%v", stack.Pop())
}
